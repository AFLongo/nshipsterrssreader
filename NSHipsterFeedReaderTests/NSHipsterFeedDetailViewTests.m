//
//  NSHipsterFeedDetailViewTests.m
//  NSHipsterFeedReader
//
//  Created by Andrea on 20/01/16.
//  Copyright © 2016 AFL. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "DetailViewController.h"

@interface NSHipsterFeedDetailViewTests : XCTestCase{
    DetailViewController *_detail;
}

@end

@implementation NSHipsterFeedDetailViewTests

- (void)setUp {
    [super setUp];
    _detail = [[DetailViewController alloc]init];
    
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testSetDetailItem {
    
    XCTAssertNoThrowSpecificNamed([_detail setDetailItem:nil], NSException, @"The nil value should be handled");
}

- (void)testPerformanceExample {
    // This is an example of a performance test case.
    [self measureBlock:^{
        // Put the code you want to measure the time of here.
    }];
}

@end
