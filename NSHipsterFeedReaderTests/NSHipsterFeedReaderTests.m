//
//  NSHipsterFeedReaderTests.m
//  NSHipsterFeedReaderTests
//
//  Created by Andrea on 19/01/16.
//  Copyright © 2016 AFL. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "MasterViewController.h"

@interface NSHipsterFeedReaderTests : XCTestCase{
    MasterViewController *_master;
}

@end

@implementation NSHipsterFeedReaderTests

- (void)setUp {
    [super setUp];

    // Put setup code here. This method is called before the invocation of each test method in the class.
    
    _master = [[MasterViewController alloc]init];
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}


- (void)testTableViewHasDataSource
{
    XCTAssertNotNil(_master.tableView.dataSource, @"Table datasource cannot be nil");
}


- (void)testDownloadFeed {
    
    [_master downloadFeed];
    XCTAssertNotNil(_master.feeds, @"the feed array should never be nil after executing the downloadFeed method");
}


- (void)testRefreshFeed{
    
    [_master refreshFeed];
    XCTAssertNotNil(_master.feeds, @"the feed array should never be nil after executing the refreshFeed method");
    
}


- (void)testPerformanceExample {
    // This is an example of a performance test case.
    [self measureBlock:^{
        [_master downloadFeed];
        [_master refreshFeed];
    }];
}

@end
