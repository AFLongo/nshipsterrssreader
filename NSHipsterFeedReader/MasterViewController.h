//
//  MasterViewController.h
//  NSHipsterFeedReader
//
//  Created by Andrea on 19/01/16.
//  Copyright © 2016 AFL. All rights reserved.
//

#import <UIKit/UIKit.h>

@class DetailViewController;

@interface MasterViewController : UITableViewController <NSXMLParserDelegate>

@property (strong, nonatomic) DetailViewController *detailViewController;
@property (atomic, readwrite) NSMutableArray *feeds;

- (void) downloadFeed;
- (void) refreshFeed;

@end

