//
//  MasterViewController.m
//  NSHipsterFeedReader
//
//  Created by Andrea on 19/01/16.
//  Copyright © 2016 AFL. All rights reserved.
//

#import "MasterViewController.h"
#import "DetailViewController.h"

#define newestArticleDateKey @"newestArticleDate"

@interface MasterViewController (){
    
// information hiding: I keep this variables private.
    NSURLSession *session;
    __block NSXMLParser *parser;
    NSMutableDictionary *item;
    NSMutableString *title;
    NSMutableString *link;
    NSDate *pubDate;
    NSMutableString *pubDateString;
    NSString *element;
    
    
}

@property (atomic, readwrite) NSDate *newestArticleDate;

@end

@implementation MasterViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationItem.leftBarButtonItem = self.editButtonItem;

    UIBarButtonItem *addButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemRefresh target:self action:@selector(refreshFeed)];
    self.navigationItem.rightBarButtonItem = addButton;
    self.detailViewController = (DetailViewController *)[[self.splitViewController.viewControllers lastObject] topViewController];
    
    //After the graphic is set let's download the feed
    [self downloadFeed];
    
    _newestArticleDate = [[NSUserDefaults standardUserDefaults]objectForKey:newestArticleDateKey];
    
}

- (void)downloadFeed{
    
    NSURL *url = [NSURL URLWithString:@"http://nshipster.com/feed.xml"];
    
    if(!self.feeds){
        self.feeds = [NSMutableArray new];
    }
  
    //I could use the following method for semplicity:
    //parser = [[NSXMLParser alloc] initWithContentsOfURL:url];
    //...but I choose to use NSURLSession in order to have, in the future, more control on the network task
    
    NSURLSessionConfiguration *config = [NSURLSessionConfiguration ephemeralSessionConfiguration];
    session = [NSURLSession sessionWithConfiguration:config];
    
    NSURLSessionDataTask *dataTask = [session dataTaskWithURL:url
                                            completionHandler:^(NSData *data, NSURLResponse *response,NSError *error){
                                                
                                                if (!error) {
                                                    
                                                    parser = [[NSXMLParser alloc] initWithData:data];
                                                    [parser setDelegate:self];
                                                    [parser setShouldResolveExternalEntities:NO];
                                                    
                                                    dispatch_async(dispatch_get_main_queue(), ^{
                                                        
                                                        [self.feeds removeAllObjects];
                                                        [parser parse];
                                                    });
                                                    
                                                }
                                                else{
                                                    NSLog(@"Error occurred: %@", error.debugDescription);
                                                    //manage error
                                                }
                                            }];
    [dataTask resume];

}

- (void)viewWillAppear:(BOOL)animated {
    self.clearsSelectionOnViewWillAppear = self.splitViewController.isCollapsed;
    [super viewWillAppear:animated];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)refreshFeed{
    [self downloadFeed];
}

#pragma mark - Segues

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([[segue identifier] isEqualToString:@"showDetail"]) {
        
        NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
        
        DetailViewController *controller = (DetailViewController *)[[segue destinationViewController] topViewController];
        [controller setDetailItem:self.feeds[indexPath.row]];
        
        controller.navigationItem.leftBarButtonItem = self.splitViewController.displayModeButtonItem;
        controller.navigationItem.leftItemsSupplementBackButton = YES;
    }
}

#pragma mark - Table View

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.feeds.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];

    NSDictionary *feedItem = self.feeds[indexPath.row];
    cell.textLabel.text = [feedItem objectForKey:@"title"];
    return cell;
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        [self.feeds removeObjectAtIndex:indexPath.row];
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view.
    }
}


#pragma mark - Parser methods

- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict {
    
    element = elementName;
    
    if ([element isEqualToString:@"item"]) {
        
        item    = [[NSMutableDictionary alloc] init];
        title   = [[NSMutableString alloc] init];
        link    = [[NSMutableString alloc] init];
        pubDateString = [[NSMutableString alloc] init];
        
    }
    
}

- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName {
    
    if ([elementName isEqualToString:@"item"]) {
        
        [item setObject:title forKey:@"title"];
        [item setObject:link forKey:@"link"];
        
        NSString *dateStr = [pubDateString substringToIndex:16];
        [item setObject:dateStr forKey:@"pubDate"];
        
        [self.feeds addObject:[item copy]];
    }
}

- (void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string {
    
    if ([element isEqualToString:@"title"]) {
        [title appendString:string];
    }
    else if ([element isEqualToString:@"link"]) {
        [link appendString:string];
    }
    else if ([element isEqualToString:@"pubDate"]) {
        [pubDateString appendString:string];
    }
    
}

- (void)parserDidEndDocument:(NSXMLParser *)parser {
    
    [self.tableView reloadData];
    [self checkNewestArticleDate];
}



#pragma mark Notify when new articles have been published.

-(void)checkNewestArticleDate{
    
    //without any push notification enabled this is the best I can do.
    BOOL notifyNew = NO;
    
    for(item in self.feeds){
        
        NSDate *aDate;
        
        if(_newestArticleDate == nil){
            _newestArticleDate = [self convertStringToDate:[item objectForKey:@"pubDate"]];
        }
        
        aDate = [self convertStringToDate:[item objectForKey:@"pubDate"]];
        
        if ([aDate compare:_newestArticleDate] == NSOrderedDescending) {
            notifyNew = YES;
            _newestArticleDate = aDate;
        }
        
    }
    NSLog(@"Most recent pubblication date: %@", _newestArticleDate);
    [[NSUserDefaults standardUserDefaults]setObject:_newestArticleDate forKey:newestArticleDateKey];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    
    if(notifyNew){
        
        UIAlertController * alert=   [UIAlertController
                                      alertControllerWithTitle:@"We have some news!"
                                      message:@"New articles have been published since your last time here."
                                      preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction* ok = [UIAlertAction
                             actionWithTitle:@"OK"
                             style:UIAlertActionStyleDefault
                             handler:^(UIAlertAction * action)
                             {
                                 [alert dismissViewControllerAnimated:YES completion:nil];
                                 
                             }];
        [alert addAction:ok];
        
        [self presentViewController:alert animated:YES completion:nil];

    }
    
}


-(NSDate *)convertStringToDate:(NSString*)dateToConvert{
    
    NSLog(@"Pubblication Date: %@", dateToConvert);
    
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"EEE, dd LLL yyyy"];
    NSDate *date = [[NSDate alloc]init];
    date = [dateFormat dateFromString:dateToConvert];
    
    return date;
}



@end
