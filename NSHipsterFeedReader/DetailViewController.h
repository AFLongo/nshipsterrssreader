//
//  DetailViewController.h
//  NSHipsterFeedReader
//
//  Created by Andrea on 19/01/16.
//  Copyright © 2016 AFL. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DetailViewController : UIViewController <UIWebViewDelegate>

@property (strong, nonatomic) id detailItem;
@property (weak, nonatomic) IBOutlet UIWebView *webView;

@end

