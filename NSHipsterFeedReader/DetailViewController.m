//
//  DetailViewController.m
//  NSHipsterFeedReader
//
//  Created by Andrea on 19/01/16.
//  Copyright © 2016 AFL. All rights reserved.
//

#import "DetailViewController.h"

@interface DetailViewController ()

@end

@implementation DetailViewController

#pragma mark - Managing the detail item

- (void)setDetailItem:(id)newDetailItem {
    
    if (_detailItem != newDetailItem) {
        _detailItem = newDetailItem;
            
        // Update the view.
        [self configureView];
    }
}

- (void)configureView {
    
    // Update the user interface for the detail item.
    if (self.detailItem) {
        
        NSString *title = ([self.detailItem objectForKey:@"title"]) ? [self.detailItem objectForKey:@"title"] : @"";
        self.title = title;
        
        NSString *url = [self.detailItem objectForKey:@"link"];
        
        if(url){
            //I'm sure there is a more elegant way to solve this but the followind didn't work:
            //NSString *encodedURL= [url stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLPathAllowedCharacterSet]];
            NSString *newString = [[url componentsSeparatedByCharactersInSet:[NSCharacterSet newlineCharacterSet]] componentsJoinedByString:@""];
            NSString *encodedURL = [newString stringByReplacingOccurrencesOfString:@" " withString:@""];
            
            
            [self.webView setDelegate:self];
            [self.webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:encodedURL]]];
        }
    }
    else{
        
        UIAlertController * alert=   [UIAlertController
                                      alertControllerWithTitle:@"Ops! Unexpected Error"
                                      message:@"Please try again"
                                      preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction* ok = [UIAlertAction
                             actionWithTitle:@"OK"
                             style:UIAlertActionStyleDefault
                             handler:^(UIAlertAction * action)
                             {
                                 [alert dismissViewControllerAnimated:YES completion:nil];
                             }];
        [alert addAction:ok];
        [self presentViewController:alert animated:YES completion:nil];
    }
}



-(void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error{
    
    UIAlertController * alert= [UIAlertController alertControllerWithTitle:@"Ops! Unexpected Error"
                                                                   message:[NSString stringWithFormat:@"Error type: %@ \n Error code: %li", error.domain, (long)error.code]
                                                            preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* ok = [UIAlertAction
                         actionWithTitle:@"Retry"
                         style:UIAlertActionStyleDefault
                         handler:^(UIAlertAction * action)
                         {
                             [alert dismissViewControllerAnimated:YES completion:nil];
                             [self configureView];
                         }];
    
    UIAlertAction* cancel = [UIAlertAction
                             actionWithTitle:@"Cancel"
                             style:UIAlertActionStyleDefault
                             handler:^(UIAlertAction * action)
                             {
                                 [alert dismissViewControllerAnimated:YES completion:nil];
                             }];
    
    [alert addAction:ok];
    [alert addAction:cancel];
    
    [self presentViewController:alert animated:YES completion:nil];
}


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    [self configureView];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
